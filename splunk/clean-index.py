import subprocess
import os

"""
To run the script run the following in the CLI:
python3.6 clean-index.py
"""

def main():
    # Get indexes
    indexes = os.listdir('/opt/splunk/hotwarm')
    for index in indexes:
        print("Removing data for {}".format(index))
        cleanIndex(index)

def cleanIndex(index):
    # Removes all event data for every index
    subprocess.run([
        "/opt/splunk/bin/splunk",
        "clean",
        "eventdata",
        "--index",
        index
    ])

if __name__ == "__main__":
    main()