import subprocess
import sys
from time import sleep

"""
To run the script run the following in the CLI:
python3.6 migrate.py indexer_name

Example:
python3.6 migrate.py indexer01.example.com
"""

def main():

    indexer = sys.argv[1]

    # List of indexes to migrate to new indexers. We didn't copy over legacy indexes, so we manually generated a list
    indexes = [
        'list of indexes'
    ]

    # Looping through every index and rolling any hot buckets to warm
    print("Rolling hot buckets to warm ")
    sleep(5)
    for index in indexes:
        hotToWarm(index)

    # Looping through every index that was rolled to warm and rsyncing to new indexer
    print("Rsyncing buckets to new indexer")
    sleep(5)
    for index in indexes:
        rsyncBuckets(indexer, index)

def hotToWarm(index):
    """
    Rolls all hot buckets to warm
    """
    index_path = "/data/indexes/" + index + "/roll-hot-buckets"
    subprocess.run(["/opt/splunk/bin/splunk", "_internal", "call", index_path])

def rsyncBuckets(indexer, index):
    """
    Rsyncs all warm buckets to the indexer provided
    """
    src_index_path = "/opt/splunk/var/lib/splunk/" + index + "/db"
    dst_index_path = "/opt/splunk/hotwarm/" + index + "/db"
    exclude_path = "/opt/splunk/var/lib/splunk/" + index + "/db/hot*"
    subprocess.run([
        "rsync",
        "-avzhe",
        "-progress",
        "ssh",
        src_index_path,
        "root@{}".format(indexer),
        dst_index_path,
        "--exclude",
        exclude_path
    ])

if __name__ == "__main__":
    main()
