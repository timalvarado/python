import subprocess

def main():

    # List of indexes to migrate to new indexers. We didn't copy over legacy indexes, so we manually generated a list
    indexes = [
        "list of indexes"
    ]

    for index in indexes:
        rsyncColdbuckets(index)

def rsyncColdbuckets(index):
    # Will change path once I have mounted the VV
    src_index_path = "/mnt/data/cold" + index
    dst_index_path = "/opt/splunk/cold"
    subprocess.run([
        "rsync",
        "-avzhe",
        "--progress",
        src_index_path,
        dst_index_path
    ])
