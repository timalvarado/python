import argparse
import os
import subprocess

from datetime import datetime
from shlex import quote

def main():
    parser = argparse.ArgumentParser(
        description='Thaw frozen buckets in Splunk'
    )
    parser.add_argument(
        '-s',
        '--source',
        required=True,
        type=str,
        help='Source directory of the frozen buckets'
    )
    parser.add_argument(
        '-d',
        '--destination',
        required=True,
        type=str,
        help='Destination to the thaweddb directory'
    )
    parser.add_argument(
        '--startdate',
        required=True,
        type=valid_startdate,
        help='The day you want the first bucket to be from - format YYYY-MM-DD'
    )
    parser.add_argument(
        '--enddate',
        required=True,
        type=valid_enddate,
        help='The day you want the last bucket to be from - format YYYY-MM-DD'
    )
    parser.add_argument(
        '-t',
        '--thaw',
        help='Thaws buckets after moving them to thaweddb'
    )

    args = parser.parse_args()
    print('Start Date: {}\tEnd Date: {}\n'.format(args.startdate, args.enddate))

    if os.path.isdir(args.source) and os.path.isdir(args.destination):
        thawBuckets(args.source, args.destination, args.startdate, args.enddate)

def valid_startdate(startdate):
    try:
        return datetime.strptime(startdate + ' ' + '00:00', '%Y-%m-%d %H:%M')
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(startdate)
        raise argparse.ArgumentTypeError(msg)

def valid_enddate(enddate):
    try:
        return datetime.strptime(enddate + ' ' + '23:59', '%Y-%m-%d %H:%M')
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(enddate)
        raise argparse.ArgumentTypeError(msg)

def thawBuckets(src, dst, starttime, endtime):
    '''
    Copies over and thaws frozen buckets to the thaweddb that are in the time frame provided.
    '''

    buckets = sorted(os.listdir(src))
    print('Copying over and thawing the following buckets...')
    for bucket in buckets:
        bucketSplit = bucket.split('_')
        newesttime = datetime.fromtimestamp(int(bucketSplit[1]))
        oldesttime = datetime.fromtimestamp(int(bucketSplit[2]))
        if oldesttime >= starttime and newesttime <= endtime:
            print('Bucket: '{}'.format(bucket))
            subprocess.run([
                "cp",
                "-R",
                "{}".format(src + bucket),
                dst
            ])
            subprocess.run([
                "/opt/splunk/bin/splunk rebuild {}".format(dst + bucket)
            ])


if __name__ == '__main__':
    main()
